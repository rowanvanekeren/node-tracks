import puppeteer from 'puppeteer';

export async function startBrowser() {
	let browser;
	try {
	    console.log("Start browser");
	    browser = await puppeteer.launch({
	        //headless: false,
	    });
	} catch (err) {
	    console.log("Could not create a browser instance => : ", err);
	}
	return browser;
}

export async function scraper(browser) {
  
}